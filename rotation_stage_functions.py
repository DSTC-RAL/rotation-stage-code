# -*- coding: utf-8 -*-
"""
@author: Rebecca Harwin
Copyright (C) 2022  Disruptive Space Technology Centre group from STFC RAL Space

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>

----------------------------------
These functions are used to control the Elliptec Rotation Stages from Thorlabs https://www.thorlabs.com/newgrouppage9.cfm?objectgroup_id=12829
PySerial is used for interfacing with the rotation stages
The code includes some functions adapted from https://github.com/cdbaird/TL-rotation-control
Commands and data are sent to and from the stages using the ELLx Elliptec Thorlabs communications protocol
Each message starts with a fixed-length, 3-byte message header, followed by an optional variable-length data packet
The possible messages are detailed in the manual https://www.thorlabs.com/_sd.cfm?fileName=ETN032283-D03.pdf&partNumber=Ell6
All positions are in radians
"""

import numpy as np
import serial
import serial.tools.list_ports as lp

def rs_init(rs_port, address):
    """
    Sends a message to the rotation stage to ask for basic infomation
    Uses the 'parse' function to interpret the message
    Prints then returns the information received as a dictionary structure
    Parameters
    ----------
    rs_port : a string, 'COMn', where n is the number of the communication port
    address: initial address of the rotation stage. Most stages self-initialise with address 0
    
    Returns
    ----------
    ser: object representing the serial interface
    info: a dictionary structure containing rotation stage details

    """
    ser = serial.Serial(port=rs_port) # set up the object representing the serial interface
    print(ser)
    ser.reset_input_buffer() # reset the buffers on the serial ports
    ser.reset_output_buffer()
    ser.write(address.encode('utf8')+b'in\r\n') # request basic information from the rotation stage
    msg = ser.readline() # read back the reply
    info = parse(msg) # use the parse function to translate the message and produce the dict
    print(info)
    return ser, info

def rs_get_rotation_stage_status(ser, address):
    """
    Sends a message to the rotation stage to check its status - i.e. has it executed all the commands it's been sent?
    Prints the response, but does not return anything.
    """
    ser.reset_input_buffer()
    ser.reset_output_buffer()
    ser.write(address.encode('utf8')+b'gs\r\n')
    msg = ser.readline()
    print(parse(msg))
    
def rs_change_port_address(ser, address_0, address_1):
    """
    Changes the address of the rotation stage from address_0 to address_1.
    This address is the first byte in all messages sent to the stage.
    It can be used to identify which stage a message is for, if there are multiple stages.
    """
    ser.reset_input_buffer()
    ser.reset_output_buffer()
    ser.write(address_0.encode('utf8')+b'ca'+address_1.encode('utf8') +b'\r\n')
    msg = ser.readline()
    print(parse(msg))
    
def rs_move_stage_home(ser, address, info=None):
    """
    Move stage to its home position, and return the updated position of the stage.
    """
    ser.reset_input_buffer()
    ser.reset_output_buffer()
    ser.write(address.encode('utf8')+b'ho0\r\n') # ask to move to the home position
    msg = ser.readline() # when the stage is asked to move to a position, it sends its final position in reply
    #print(parse(msg, info))
    msg_check = msg.decode().strip()
    code = msg_check[1:3] # check the information returned relates to the position
    if code.upper() == 'PO':
        code, position = parse(msg, info)
        return position
            
def rs_move_to_position(ser, address, position, info):
    """
    Move stage to a given position (in radians) and return the updated position of the stage
    """
    ser.reset_input_buffer()
    ser.reset_output_buffer()
    encoder_position = rad_to_hex(info, position) # convert the required angle to an encoder position
    ser.write(address.encode('utf8') + b'ma' + encoder_position.encode('utf8') + b'\r\n') # ask to move to this angle
    msg = ser.readline()
    msg_check = msg.decode().strip()
    code = msg_check[1:3] # check the information returned relates to the position
    if code.upper() == 'PO':
        code, position = parse(msg, info)
        return position

def rs_get_rotation_stage_position(ser, address, info):
    """
    Sends a message to the rotation stage to check its position.
    Returns the current position
    """
    ser.reset_input_buffer()
    ser.reset_output_buffer()
    ser.write(address.encode('utf8')+b'gp\r\n') # request position
    msg = ser.readline()
    msg_check = msg.decode().strip()
    code = msg_check[1:3] # check that the information given actually relates to the position
    if code.upper() == 'PO':
        code, position = parse(msg, info)
        return position

def parse(msg, info=None):
    """
    Parse a message from the rotation stage. 
    If the basic information from the rotation stage is known, this can optionally be passed to the function.
    """
    if (not msg.endswith(b'\r\n') or (len(msg) == 0)):
        print('Status/Response may be incomplete!')
        return None
    msg = msg.decode().strip()
    code = msg[1:3] # this gives information about the contents of the message
    
    try: 
        addr = int(msg[0], 16)
    except ValueError:
        raise ValueError('Invalid Address: %s' % msg[0])

    if (code.upper() == 'IN'):
        # if the message contains information about the rotation stage, return this information in a dict structure
        info = {'Address' : str(addr),
            'Motor Type' : msg[3:5],
            'Serial No.' : msg[5:13],
            'Year' : msg[13:17],
            'Firmware' : msg[17:19],
            'Thread' : is_metric(msg[19]),
            'Hardware' : msg[20],
            'Range' : (int(msg[21:25], 16))*2*np.pi/360,
            'Pulse/Rev' : (int(msg[25:], 16))}
        info['Conversion Factor'] = float(info['Pulse/Rev'])/float(info['Range'])
        return info

    elif ((code.upper() == 'PO') or code.upper() == 'BO'):
        # if the message contains positional information
        pos = msg[3:]
        if not info:
            return (code, (s32(int(pos, 16))))
        else:
            return (code, hex_to_rad(info, pos))

    elif (code.upper() == 'GS'):
        # if the message contains status information
        errcode = msg[3:]
        return (code, str(int(errcode, 16)))

    else:
        return (code, msg[3:])
    
def is_metric(num):
    """
    Helper function for parse, to check the thread type of the rotation stage
    """
    if (num == '0'):
        thread_type = 'Metric'
    elif(num == '1'):
        thread_type = 'Imperial'
    else:
        thread_type = None

    return thread_type

def s32(value): # Convert 32bit signed hex to int
    return -(value & 0x80000000) | (value & 0x7fffffff)

def hex32(n): # Convert int to 32bit signed hex
    return "0x%s"%("00000000%x"%(n&0xffffffff))[-8:]

def rad_to_hex(info, rad): # Convert radians to a 32bit signed hex
    val = hex32(int(rad*info['Conversion Factor']))
    return val.replace('0x', '').zfill(8).upper()

def hex_to_rad(info, hexval): # Convert a 32bit signed hex to radians
    val = (s32(int(hexval, 16)))/info['Conversion Factor']
    return val

def check_ports():
    """
    Prints a list of all serial ports available
    """
    for port in lp.comports():
        print(port.serial_number)
