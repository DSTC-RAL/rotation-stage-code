# -*- coding: utf-8 -*-
"""
@author: Rebecca Harwin
Copyright (C) 2022  Disruptive Space Technology Centre group from STFC RAL Space

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>

--------------------------
Imports rotation_stage_functions.py to control a Thorlabs Elliptec rotation stage
This code moves the rotation stage between two positions
"""
import numpy as np
import matplotlib.pyplot as plt
import serial
import time
import serial.tools.list_ports as lp
from rotation_stage_functions import *

check_ports()


# if you're running a Windows PC, the COM address of the port can be found under Device Manager > Ports(COM & LPT)
# open up Device Manager, then plug the stage in - the number of the new port should be updated below
# alternatively, the ELLO software from Thorlabs can be used to find the correct port and address for the stage
rs_port = 'COM4'
address = '0' # unless someone has changed the settings, the address for the stage should be 0

# this command sets up the stage and its serial port
ser, info = rs_init(rs_port, address)

# moves the rotation stage to its home position
pos = rs_move_stage_home(ser, address, info)

# set up positions for a rotation experiment. this code moves the stage between two positions
delay = 0.2 # time between changing rotation stage positions
run_time = 0.2*60 # time you want the stage to move for, in seconds
step = 0.0002*np.pi # angle change of the stage, in radians

times = []
positions = []

t0 = time.perf_counter() 
while time.perf_counter() - t0 < run_time:
    pos = rs_move_to_position(ser, address, step, info)
    pos = rs_move_stage_home(ser, address, info)
    times.append(time.perf_counter() - t0)
    positions.append(pos)
    time.sleep(delay)
    
ser.close() # this closes the serial interface 

plt.figure()
plt.plot(times, positions)
plt.xlabel('t/s')
plt.ylabel('Position/rad')

