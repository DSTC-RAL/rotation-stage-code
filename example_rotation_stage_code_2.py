# -*- coding: utf-8 -*-
"""
@author: Rebecca Harwin
Copyright (C) 2022  Disruptive Space Technology Centre group from STFC RAL Space

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>

--------------------------
Imports rotation_stage_functions.py to control a Thorlabs Elliptec rotation stage
This code moves the rotation stage to all of the positions in an array
"""
import numpy as np
import matplotlib.pyplot as plt
import serial
import time
import serial.tools.list_ports as lp
from rotation_stage_functions import *

check_ports()


# if you're running a Windows PC, the COM address of the port can be found under Device Manager > Ports(COM & LPT)
# open up Device Manager, then plug the stage in - the number of the new port should be updated below
# unless someone has changed the settings, the address for the stage should be 0
# alternatively, the ELLO software from Thorlabs can be used to find the correct port and address for the stage
rs_port = 'COM4'
address = '0'

# this command sets up the stage and its serial port
ser, info = rs_init(rs_port, address)

#moves the rotation stage to its home position
pos = rs_move_stage_home(ser, address, info)

# set up positions for a rotation experiment. this code moves the stage between two positions
delay = 0.2 # time between changing rotation stage positions
number_points = 10 # number of points to use
start_position = 0
end_position = 0.1*np.pi
disturbance_positions = np.linspace(start_position, end_position, number_points) # syntax is start position (in radians), end position(in radians), number of steps
times = np.zeros([number_points])
positions = np.zeros([number_points])
t0 = time.perf_counter()

for i in range(disturbance_positions.shape[0]):
    pos = rs_move_to_position(ser, address, disturbance_positions[i], info)    
    times[i] = time.perf_counter()-t0
    positions[i] = pos
    time.sleep(delay)
    

rs_move_stage_home(ser, address, info)

ser.close() # this closes the serial interface 

plt.figure()
plt.plot(times, positions)
plt.xlabel('t/s')
plt.ylabel('Position/rad')


