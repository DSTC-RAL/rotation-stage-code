Code to control a Thorlabs Elliptec rotation stage
--------------------------------------------------

Copyright (C) 2022  Disruptive Space Technology Centre group from STFC RAL Space

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>

About the files:

Custom functions are all in the rotation_stage_functions.py script, which is loaded by each of the example scripts

Example_rotation_stage_code_1.py moves a stage backwards and forwards between two positions for a set amount of time.

Example_rotation_stage_code_2.py moves a stage from one position to another position in a set number of steps.

About the code:

PySerial is used for interfacing with the rotation stages

The code includes some functions adapted from https://github.com/cdbaird/TL-rotation-control

Commands and data are sent to and from the stages using the ELLx Elliptec Thorlabs communications protocol

Each message starts with a fixed-length, 3-byte message header, followed by an optional variable-length data packet

The possible messages are detailed in the manual https://www.thorlabs.com/_sd.cfm?fileName=ETN032283-D03.pdf&partNumber=Ell6

All positions are in radians
